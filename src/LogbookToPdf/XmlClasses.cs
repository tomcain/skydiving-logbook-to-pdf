﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogbookToPdf
{
    [XmlRoot(ElementName = "logbook_history", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Logbook_history
    {
        [XmlElement(ElementName = "freefall_time", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Freefall_time { get; set; }

        [XmlElement(ElementName = "cutaways", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Cutaways { get; set; }
    }

    [XmlRoot(ElementName = "rigs", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Rigs
    {
        [XmlElement(ElementName = "rig_id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<string> Rig_id { get; set; }

        [XmlElement(ElementName = "rig", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<Rig> Rig { get; set; }
    }

    [XmlRoot(ElementName = "log_entry", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Log_entry
    {
        [XmlElement(ElementName = "id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Id { get; set; }

        [XmlElement(ElementName = "jump_number", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Jump_number { get; set; }

        [XmlElement(ElementName = "date", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Date { get; set; }

        [XmlElement(ElementName = "location_id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Location_id { get; set; }

        [XmlElement(ElementName = "aircraft_id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Aircraft_id { get; set; }

        [XmlElement(ElementName = "rigs", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Rigs Rigs { get; set; }

        [XmlElement(ElementName = "skydive_type_id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Skydive_type_id { get; set; }

        [XmlElement(ElementName = "exit_altitude", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Exit_altitude { get; set; }

        [XmlElement(ElementName = "deployment_altitude", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Deployment_altitude { get; set; }

        [XmlElement(ElementName = "altitude_unit", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Altitude_unit { get; set; }

        [XmlElement(ElementName = "freefall_time", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Freefall_time { get; set; }

        [XmlElement(ElementName = "distance_to_target", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Distance_to_target { get; set; }

        [XmlElement(ElementName = "cutaway", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Cutaway { get; set; }

        [XmlElement(ElementName = "notes", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Notes { get; set; }

        [XmlElement(ElementName = "images", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Images { get; set; }

        [XmlElement(ElementName = "signature_id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Signature_id { get; set; }

        [XmlElement(ElementName = "last_modified_utc", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Last_modified_utc { get; set; }

        [XmlElement(ElementName = "last_signature_utc", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Last_signature_utc { get; set; }
    }

    [XmlRoot(ElementName = "log_entries", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Log_entries
    {
        [XmlElement(ElementName = "log_entry", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<Log_entry> Log_entry { get; set; }
    }

    [XmlRoot(ElementName = "signature", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Signature
    {
        [XmlElement(ElementName = "id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Id { get; set; }

        [XmlElement(ElementName = "license", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string License { get; set; }

        [XmlElement(ElementName = "image", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Image { get; set; }
    }

    [XmlRoot(ElementName = "signatures", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Signatures
    {
        [XmlElement(ElementName = "signature", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<Signature> Signature { get; set; }
    }

    [XmlRoot(ElementName = "skydive_type", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Skydive_type
    {
        [XmlElement(ElementName = "id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Name { get; set; }

        [XmlElement(ElementName = "default", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Default { get; set; }

        [XmlElement(ElementName = "active", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Active { get; set; }

        [XmlElement(ElementName = "last_modified_utc", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Last_modified_utc { get; set; }

        [XmlElement(ElementName = "freefall_profile", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Freefall_profile { get; set; }
    }

    [XmlRoot(ElementName = "skydive_types", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Skydive_types
    {
        [XmlElement(ElementName = "skydive_type", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<Skydive_type> Skydive_type { get; set; }
    }

    [XmlRoot(ElementName = "location", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Location
    {
        [XmlElement(ElementName = "id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Name { get; set; }

        [XmlElement(ElementName = "home", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Home { get; set; }

        [XmlElement(ElementName = "active", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Active { get; set; }

        [XmlElement(ElementName = "last_modified_utc", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Last_modified_utc { get; set; }

        [XmlElement(ElementName = "notes", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Notes { get; set; }
    }

    [XmlRoot(ElementName = "locations", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Locations
    {
        [XmlElement(ElementName = "location", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<Location> Location { get; set; }
    }

    [XmlRoot(ElementName = "aircraft", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Aircraft
    {
        [XmlElement(ElementName = "id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Name { get; set; }

        [XmlElement(ElementName = "default", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Default { get; set; }

        [XmlElement(ElementName = "active", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Active { get; set; }

        [XmlElement(ElementName = "last_modified_utc", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Last_modified_utc { get; set; }
    }

    [XmlRoot(ElementName = "aircrafts", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Aircrafts
    {
        [XmlElement(ElementName = "aircraft", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<Aircraft> Aircraft { get; set; }
    }

    [XmlRoot(ElementName = "rig", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Rig
    {
        [XmlElement(ElementName = "id", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Name { get; set; }

        [XmlElement(ElementName = "primary", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Primary { get; set; }

        [XmlElement(ElementName = "archived", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Archived { get; set; }

        [XmlElement(ElementName = "active", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Active { get; set; }

        [XmlElement(ElementName = "last_modified_utc", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Last_modified_utc { get; set; }

        [XmlElement(ElementName = "components", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Components Components { get; set; }

        [XmlElement(ElementName = "notes", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Notes { get; set; }

        [XmlElement(ElementName = "reminders", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Reminders Reminders { get; set; }
    }

    [XmlRoot(ElementName = "component", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Component
    {
        [XmlElement(ElementName = "name", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Name { get; set; }

        [XmlElement(ElementName = "serial_number", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Serial_number { get; set; }

        [XmlElement(ElementName = "notes", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Notes { get; set; }
    }

    [XmlRoot(ElementName = "components", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Components
    {
        [XmlElement(ElementName = "component", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public List<Component> Component { get; set; }
    }

    [XmlRoot(ElementName = "reminder", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Reminder
    {
        [XmlElement(ElementName = "name", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Name { get; set; }

        [XmlElement(ElementName = "interval", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Interval { get; set; }

        [XmlElement(ElementName = "interval_unit", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Interval_unit { get; set; }

        [XmlElement(ElementName = "last_completed_date", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public string Last_completed_date { get; set; }
    }

    [XmlRoot(ElementName = "reminders", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Reminders
    {
        [XmlElement(ElementName = "reminder", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Reminder Reminder { get; set; }
    }

    [XmlRoot(ElementName = "skydiving_logbook", Namespace = "http://www.skydiving-logbook.org/logbook")]
    public class Skydiving_logbook
    {
        [XmlElement(ElementName = "logbook_history", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Logbook_history Logbook_history { get; set; }

        [XmlElement(ElementName = "log_entries", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Log_entries Log_entries { get; set; }

        [XmlElement(ElementName = "signatures", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Signatures Signatures { get; set; }

        [XmlElement(ElementName = "skydive_types", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Skydive_types Skydive_types { get; set; }

        [XmlElement(ElementName = "locations", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Locations Locations { get; set; }

        [XmlElement(ElementName = "aircrafts", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Aircrafts Aircrafts { get; set; }

        [XmlElement(ElementName = "rigs", Namespace = "http://www.skydiving-logbook.org/logbook")]
        public Rigs Rigs { get; set; }

        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}
