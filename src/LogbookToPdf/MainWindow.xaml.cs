﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using Microsoft.Win32;

namespace LogbookToPdf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnBrowseClick(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            var dlg = new OpenFileDialog
            {
                DefaultExt = ".xml",
                Filter = "XML Files (*.xml)|*.xml|All Files (*.*)|*.*"
            };

            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();
            if (result.HasValue && result.Value)
            {
                InputFileField.Text = dlg.FileName;
                OutputFileField.Text = GetOutputFilePath(dlg.FileName);
            }
        }

        private async void OnConvertClick(object sender, RoutedEventArgs e)
        {
            // paths
            var inputFilePath = InputFileField.Text;
            var outputFilePath = OutputFileField.Text;

            // try to delete output file
            if (File.Exists(outputFilePath) && !TryDelete(outputFilePath))
            {
                MessageBox.Show(
                    "Cound not open file for writing: " + outputFilePath,
                    "Error", 
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            // disable convert button
            ConvertButton.IsEnabled = false;

            // generate pdf on background
            await Task.Run(() =>
            {
                // open files
                var inputFile = File.Open(inputFilePath, FileMode.Open);
                var outputFile = File.Create(outputFilePath);

                // de-serialize xml
                var serializer = new XmlSerializer(typeof(Skydiving_logbook));
                var logbook = (Skydiving_logbook)serializer.Deserialize(inputFile);

                inputFile.Close();

                // generate pdf
                var generator = new XmlToPdfConverter(logbook);
                generator.Progress += OnProgress;
                generator.WritePdf(logbook, outputFile);
                generator.Progress -= OnProgress;

                outputFile.Close();
            });

            // renable convert button
            ConvertButton.IsEnabled = true;
        }

        private void OnProgress(ProgressEventArgs args)
        {
            Dispatcher.Invoke(() =>
            {
                ProgressTextField.AppendText(args.Message + Environment.NewLine);
                ProgressTextField.ScrollToEnd();
            });
        }

        private string GetOutputFilePath(string inputFilePath)
        {
            return
                Path.GetDirectoryName(Path.GetFullPath(inputFilePath)) +
                Path.DirectorySeparatorChar +
                Path.GetFileNameWithoutExtension(inputFilePath) +
                ".pdf";
        }

        private static bool TryDelete(string outputFilePath)
        {
            try
            {
                File.Delete(outputFilePath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
