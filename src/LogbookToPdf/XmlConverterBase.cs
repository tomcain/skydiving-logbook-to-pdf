﻿using System;
using iTextSharp.text;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogbookToPdf
{
    public class XmlConverterBase
    {
        private static readonly int IMAGE_HEIGHT = 80;

        // lookups
        private readonly Dictionary<string, Location> _locations;
        private readonly Dictionary<string, Aircraft> _aircrafts;
        private readonly Dictionary<string, Skydive_type> _skydiveTypes;
        private readonly Dictionary<string, Rig> _rigs;
        private readonly Dictionary<string, Signature> _signatures;
        private readonly Dictionary<string, Image> _signatureImages;

        // logbook
        private readonly Skydiving_logbook _logbook;

        public XmlConverterBase(Skydiving_logbook logbook)
        {
            _logbook = logbook;

            // load reference data
            _locations = GetLocations(logbook);
            _aircrafts = GetAircrafts(logbook);
            _skydiveTypes = GetSkydiveTypes(logbook);
            _rigs = GetRigs(logbook);
            _signatures = GetSignatures(logbook);
            _signatureImages = new Dictionary<string, Image>();
        }

        public List<Log_entry> GetLogEntries()
        {
            return _logbook.Log_entries.Log_entry;
        }

        public string GetLocationName(Log_entry logEntry)
        {
            if (string.IsNullOrEmpty(logEntry.Location_id) || !_locations.ContainsKey(logEntry.Location_id))
            {
                return "";
            }
            return _locations[logEntry.Location_id].Name;
        }

        public string GetAircraftName(Log_entry logEntry)
        {
            if (string.IsNullOrEmpty(logEntry.Aircraft_id) || !_aircrafts.ContainsKey(logEntry.Aircraft_id))
            {
                return "";
            }
            return _aircrafts[logEntry.Aircraft_id].Name;
        }

        public string GetRigNames(Log_entry logEntry, string separator)
        {
            var rigsstring = new StringBuilder();

            var rigIds = logEntry.Rigs.Rig_id;
            for (var i = 0; i < rigIds.Count; i++)
            {
                rigsstring.Append(_rigs[rigIds[i]].Name);
                // append separator
                if (i < rigIds.Count - 1)
                {
                    rigsstring.Append(separator);
                }
            }

            return rigsstring.ToString();
        }

        public string GetSkydiveTypeName(Log_entry logEntry)
        {
            if (string.IsNullOrEmpty(logEntry.Skydive_type_id) || !_skydiveTypes.ContainsKey(logEntry.Skydive_type_id))
            {
                return "";
            }
            return _skydiveTypes[logEntry.Skydive_type_id].Name;
        }

        public string GetAltitudeUnitName(Log_entry logEntry)
        {
            return logEntry.Altitude_unit;
        }

        public Signature GetSignature(Log_entry logEntry)
        {
            if (string.IsNullOrEmpty(logEntry.Signature_id))
                return null;
            if (!_signatures.ContainsKey(logEntry.Signature_id))
                return null;
            return _signatures[logEntry.Signature_id];
        }

        public Image GetSignatureImage(Signature signature)
        {
            if (string.IsNullOrEmpty(signature.Image))
                return null;

            if (!_signatureImages.ContainsKey(signature.Id))
            {
                // get signature
                var imageData = Convert.FromBase64String(signature.Image.Trim());
                var image = Image.GetInstance(imageData);
                image.ScalePercent(100 * IMAGE_HEIGHT / image.Height);
                // add to map
                _signatureImages.Add(signature.Id, image);
            }

            return _signatureImages[signature.Id];
        }

        public string FormatInt(string intAsString)
        {
            var parsed = int.Parse(intAsString);
            return parsed.ToString("N0");
        }

        private Dictionary<string, Location> GetLocations(Skydiving_logbook logbook)
        {
            return logbook.Locations.Location.ToDictionary(location => location.Id);
        }

        private Dictionary<string, Aircraft> GetAircrafts(Skydiving_logbook logbook)
        {
            return logbook.Aircrafts.Aircraft.ToDictionary(aircraft => aircraft.Id);
        }

        private Dictionary<string, Skydive_type> GetSkydiveTypes(Skydiving_logbook logbook)
        {
            return logbook.Skydive_types.Skydive_type.ToDictionary(t => t.Id);
        }

        private Dictionary<string, Rig> GetRigs(Skydiving_logbook logbook)
        {
            return logbook.Rigs.Rig.ToDictionary(r => r.Id);
        }

        private Dictionary<string, Signature> GetSignatures(Skydiving_logbook logbook)
        {
            return logbook.Signatures.Signature.ToDictionary(s => s.Id);
        }
    }
}
