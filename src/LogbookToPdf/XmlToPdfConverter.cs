﻿using System;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace LogbookToPdf
{
    public class XmlToPdfConverter: XmlConverterBase
    {
        // fonts
        private readonly Font _headerFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8);
        private readonly Font _textFont = FontFactory.GetFont(FontFactory.HELVETICA, 8);
        private readonly Font _cutawayFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, BaseColor.WHITE);

        public delegate void ProgressEvent(ProgressEventArgs args);
        public event ProgressEvent Progress;

        public XmlToPdfConverter(Skydiving_logbook logbook) : base(logbook)
        {
        }

        public void WritePdf(Skydiving_logbook logbook, Stream outputStream)
        {
            var document = new Document();
            PdfWriter.GetInstance(document, outputStream);
            document.Open();

            var entries = logbook.Log_entries.Log_entry.ToList();
            entries.Reverse();
            ReportProgress($"Adding {entries.Count} log entries...");
            var currentIndex = 0;
            foreach (var logEntry in entries)
            {
                // print every 10 entries
                if (currentIndex % 10 == 0)
                    ReportProgress(
                        $"Adding log entries {currentIndex + 1} thru {Math.Min(currentIndex + 10, entries.Count)}");
                currentIndex++;

                // add cutaway
                AddLogEntryCutaway(document, logEntry);

                // add main fields
                AddLogEntryFields(document, logEntry);

                // add notes table
                AddLogEntryNotes(document, logEntry);

                // add signature
                AddLogEntrySignature(document, logEntry);

                // add line break
                document.Add(new Paragraph("\n"));
            }

            // close document
            document.Close();

            ReportCompleted("PDF Generation Completed");
        }

        private void AddLogEntryFields(Document document, Log_entry logEntry)
        {
            // create pdf table
            PdfPTable table = new PdfPTable(5);

            // add first header
            table.AddCell(CreateHeaderCell("Jump #"));
            table.AddCell(CreateHeaderCell("Date"));
            table.AddCell(CreateHeaderCell("Drop Zone"));
            table.AddCell(CreateHeaderCell("Aircraft"));
            table.AddCell(CreateHeaderCell("Gear"));

            // add fields
            table.AddCell(CreateTextCell(FormatInt(logEntry.Jump_number)));
            table.AddCell(CreateTextCell(logEntry.Date));
            table.AddCell(CreateTextCell(GetLocationName(logEntry)));
            table.AddCell(CreateTextCell(GetAircraftName(logEntry)));
            table.AddCell(CreateTextCell(GetRigNames(logEntry, ", ")));

            // add second header
            table.AddCell(CreateHeaderCell("Jump Type"));
            table.AddCell(CreateHeaderCell($"Exit Alt ({GetAltitudeUnitName(logEntry)})"));
            table.AddCell(CreateHeaderCell($"Depl Alt ({GetAltitudeUnitName(logEntry)})"));
            table.AddCell(CreateHeaderCell("Delay (s)"));
            // one empty cell
            table.AddCell(CreateHeaderCell(""));

            // add fields
            table.AddCell(CreateTextCell(GetSkydiveTypeName(logEntry)));
            table.AddCell(CreateTextCell(FormatInt(logEntry.Exit_altitude)));
            table.AddCell(CreateTextCell(FormatInt(logEntry.Deployment_altitude)));
            table.AddCell(CreateTextCell(FormatInt(logEntry.Freefall_time)));
            table.AddCell(CreateTextCell(""));

            // add table to document
            document.Add(table);
        }

        private void AddLogEntryNotes(Document document, Log_entry logEntry)
        {
            // create table
            var table = new PdfPTable(1);

            // add header
            table.AddCell(CreateHeaderCell("Notes", 1));

            // create/add notes cell
            var notesCell = new PdfPCell
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                MinimumHeight = 25
            };
            notesCell.AddElement(new Phrase(logEntry.Notes, _textFont));
            
            // add to table
            table.AddCell(notesCell);

            // add table to document
            document.Add(table);
        }

        private void AddLogEntrySignature(Document document, Log_entry logEntry)
        {
            // get signature
            var signature = GetSignature(logEntry);

            // if null or empty, skip
            if (string.IsNullOrEmpty(signature?.Image))
                return;

            // create table
            var table = new PdfPTable(1);

            // add signature header
            table.AddCell(CreateHeaderCell("Signature"));

            // get signature
            var image = GetSignatureImage(signature);

            // create/add signature cell
            var signatureCell = new PdfPCell();
            signatureCell.HorizontalAlignment = Element.ALIGN_CENTER;
            signatureCell.AddElement(new Phrase($"License: {signature.License}", _textFont));
            signatureCell.AddElement(image);

            // add cell to table
            table.AddCell(signatureCell);

            // add table to document
            document.Add(table);
        }

        private void AddLogEntryCutaway(Document document, Log_entry logEntry)
        {
            if (bool.Parse(logEntry.Cutaway))
            {
                // add cutaway header
                var table = new PdfPTable(1);
                table.AddCell(CreateCutawayCell());
                document.Add(table);
            }
        }

        private PdfPCell CreateHeaderCell(string text)
        {
            return CreateHeaderCell(text, 1);
        }

        private PdfPCell CreateHeaderCell(string text, int colSpan)
        {
            var phrase = new Phrase(text, _headerFont);
            var cell = new PdfPCell(phrase)
            {
                HorizontalAlignment = Element.ALIGN_CENTER,
                BackgroundColor = BaseColor.LIGHT_GRAY,
                Colspan = colSpan
            };
            return cell;
        }

        private PdfPCell CreateTextCell(string text)
        {
            var phrase = new Phrase(text, _textFont);
            var cell = new PdfPCell(phrase);
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            return cell;
        }

        private PdfPCell CreateCutawayCell()
        {
            var phrase = new Phrase("Cutaway", _cutawayFont);
            var cell = new PdfPCell(phrase)
            {
                HorizontalAlignment = Element.ALIGN_CENTER,
                BackgroundColor = BaseColor.RED
            };
            return cell;
        }

        private void ReportProgress(string message)
        {
            Progress?.Invoke(new ProgressEventArgs(message));
        }

        private void ReportCompleted(string message)
        {
            Progress?.Invoke(new ProgressEventArgs(message, true));
        }
    }
}
