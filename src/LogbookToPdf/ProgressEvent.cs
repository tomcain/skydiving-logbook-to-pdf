﻿using System;

namespace LogbookToPdf
{
    public class ProgressEventArgs: EventArgs
    {
        public ProgressEventArgs(string message): this(message, false)
        {
        }

        public ProgressEventArgs(string message, bool completed)
        {
            Message = message;
            Completed = completed;
        }

        public string Message { get; set; }

        public bool Completed { get; set; }
    }
}
